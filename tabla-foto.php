<!DOCTYPE html>
<html>
  <head>
    <title>Tabla de fotos</title>
    <link rel="stylesheet" href="estilo.css">
  </head>
  <body>
    <a class = "back" href="mainpage.html"> Volver al menu </a>
    <?php
      # se llama la carpeta de imagenes
      $dir = "img/";
      $x = 0;
      # Comprueba si la carpeta existe
      if (is_dir($dir)){
        echo "<table>";
        # Se abre la carpeta
        if ($dh = opendir($dir)){
          # Mientras exista algo en la carpeta, seguirá corriendo hasta que todo sea recorrido
          while (($file = readdir($dh)) !== false){
            # por cada cuatro columnas, se hace una fila
            if($x%4 == 0){
              echo "<tr>";
            }
            # la imagen se selecciona de la carpeta y se envia dentro de una cell
            echo "<td><img src='img/$file'></td>";
            # por cada cuatro columnas, se hace una fila
            if($x%4 == 3){
            	echo "</tr>";
            }
            $x++;
          }
          # Se cierra la carpeta
          closedir($dh);
        }
      }
      echo "</table>";
    ?>
  </body>
</html>
