<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="estilo.css">
    <title>Tabla color</title>
  </head>
  <body>
    <a class = "back" href="mainpage.html"> Volver al menu </a>
    <?php
      # valores predeterminados para realizar la tabla
      $N = 13;
      # El tamaño siempre será un cuadrado gracias a "N"
      $TAM = $N*$N;
      $x = 1;
      $tablearray = array();
      # Se llena el array del 1 al N
      while($x <= $TAM){
        array_push($tablearray, $x);
        $x++;
      }
      $x = 0;
      echo "<table>";
      while($x <= $TAM-1){
      	if($x%$N == 0){
          echo "<tr>";
        }
        if($x%2 == 0){
          # Si el numero es impar, será coloreado gris
          echo "<td style = 'background-color: grey'>$tablearray[$x]</td>";
        }
        else{
          # Si el numero es par o cero, será coloreado blanco
        	echo "<td style = 'background-color: white'>$tablearray[$x]</td>";
        }
        if($x%$N == $N-1){
        	echo "</tr>";
        }
        $x++;
      }
      echo "</table>";
    ?>
  </body>
</html>
