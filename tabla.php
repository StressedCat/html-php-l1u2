<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="estilo.css">
    <title>Tabla</title>
  </head>
  <body>
    <a class = "back" href="mainpage.html"> Volver al menu </a>
    <?php
      # valores predeterminados para realizar la tabla
      $x = 1;
      $onetohundred = array();
      # Se llena el array del 1 al 100
      while($x <= 100){
        array_push($onetohundred, $x);
        $x++;
      }
      $x = 0;
      echo "<table>";
      # se repite 100 veces del 0 al 99, donde cada numero será el numero del array
      # cada 10 bloques, se cierra el tr y luego se abre nuevamente
      while($x <= 99){
      	if($x%10 == 0){
          echo "<tr>";
        }
      	echo "<td>$onetohundred[$x]</td>";
        if($x%10 == 9){
        	echo "</tr>";
        }
        $x++;
      }
      echo "</table>";
    ?>
  </body>
</html>
