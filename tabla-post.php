<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="estilo.css">
  <title>Tabla POST</title>
</head>
<body>
  <a class = "back" href="tabla-get-post.html"> Volver al menu get-post </a>
  <?php
    # valores creados por formulario POST, las elecciones no serán visibles
    $N = $_POST["tamano"];
    $colour = $_POST["color"];
    # El tamaño siempre será un cuadrado gracias a "N"
    $TAM = $N*$N;
    $x = 1;
    $tablearray = array();
    # Se llena el array del 1 al N, el cual se consigue con get de tabla-get-post.html
    while($x <= $TAM){
      array_push($tablearray, $x);
      $x++;
    }
    $x = 0;
    echo "<table>";
    while($x <= $TAM-1){
    	if($x%$N == 0){
        echo "<tr>";
      }
      # el color depende de color origen tabla-get-post.html
      echo "<td style = 'background-color: $colour'>$tablearray[$x]</td>";
      if($x%$N == $N-1){
      	echo "</tr>";
      }
      $x++;
    }
    echo "</table>";
  ?>
</body>
</html>
